# Slack Status Changer
A small Ruby script to manage user's status and presence, using Slack's API.

## Configuration
Expects a file containing the user's Slack API token in ```~/.slack/token```
(https://api.slack.com/custom-integrations/legacy-tokens)

Uses ```~/.slack/status.yaml``` to save/read status settings. Entries format:
```
remote:
  text: Working remotely
  emoji: ":house_with_garden:"
  presence: auto
```
##  Available commands
- ```check```       Displays the user's current status
- ```save *name*``` Saves the current status under *name*
- ```list```        Lists saved status
- ```set *name*```  Sets user's status to *name*

## Usage example
```
bmterra@redfox:~$ status check

Current status:
  text: Working remotely
  emoji: :house_with_garden:
  presence: active

bmterra@redfox:~$ status save remote
Saved

Current status:
  text: Working remotely
  emoji: :house_with_garden:
  presence: active

bmterra@redfox:~$ status list

remote
resting
lunch
commuting
away
breakfast
sf-office

bmterra@redfox:~$ status set resting
Ok
bmterra@redfox:~$ status check

Current status:
  text: Resting
  emoji: :pyong:
  presence: away

bterra@redfox:~$
```

## Reference (and things to improve)
- https://api.slack.com/custom-integrations/legacy-tokens
- https://api.slack.com/docs/presence-and-status
- https://github.com/slack-ruby/slack-ruby-client
- https://github.com/slackapi/Slack-Ruby-Onboarding-Tutorial
